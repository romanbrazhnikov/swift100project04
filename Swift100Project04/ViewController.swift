//
//  ViewController.swift
//  Swift100Project04
//
//  Created by Roman Brazhnikov on 21/03/2019.
//  Copyright © 2019 Roman Brazhnikov. All rights reserved.
//

import UIKit
import WebKit

class ViewController: UIViewController, WKNavigationDelegate {

    let HTTP_PREFIX = "https://";
    
    var webView: WKWebView!
    var progressView: UIProgressView!
    
    var webSites: [String]!
    var siteToVisit: String!
    
    
    override func loadView() {
        webView = WKWebView();
        webView.navigationDelegate = self
        view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // toolbar items
        
        progressView = UIProgressView(progressViewStyle: .default)
        progressView.sizeToFit()
        let progressBar = UIBarButtonItem(customView: progressView)
        
        let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let refresh = UIBarButtonItem(barButtonSystemItem: .refresh, target: webView, action: #selector(webView.reload))
        
        // Back and forward
        let backButton = UIBarButtonItem(title: "<", style: .plain, target: self, action: #selector(goBack))
        let forwardButton = UIBarButtonItem(title: ">", style: .plain, target: self, action: #selector(goForward))
        
        // setting the buttons
        toolbarItems = [backButton, forwardButton, progressBar, space, refresh]
        navigationController?.isToolbarHidden = false
        
        webView.addObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress), options: .new, context: nil)
        
        // Initial website to show
        let url = URL(string: HTTP_PREFIX + siteToVisit)!
        let request = URLRequest(url: url)
        webView.load(request)
        webView.allowsBackForwardNavigationGestures = true
        
    }

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "estimatedProgress" {
            progressView.progress = Float(webView.estimatedProgress)
        }
    }
    
    /// Filters Access to site from the list
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        let url = navigationAction.request.url
        
        if let host = url?.host {
            for webSite in webSites {
                if host.contains(webSite) {
                    decisionHandler(.allow)
                    return
                }
            }
        }
        
        urlAccessed()
        decisionHandler(.cancel)
    }
    
    func urlAccessed() {
        // show an alert
        let message = "You cannot open site that are not in the allowed list"
        let ac = UIAlertController(title: "Access denied", message: message, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "Ok", style: .default))
        
        // for iPads
        ac.popoverPresentationController?.barButtonItem = navigationItem.rightBarButtonItem
        
        present(ac, animated: true)
    }

    @objc func goBack() {
        webView.goBack()
    }

    @objc func goForward() {
        webView.goForward()
    }
    
    func openPage(action: UIAlertAction) {
        let url = URL(string: HTTP_PREFIX + action.title!)!
        webView.load(URLRequest(url: url))
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        title = webView.title
    }
}

