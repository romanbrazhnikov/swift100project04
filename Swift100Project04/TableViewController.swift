//
//  TableViewController.swift
//  Swift100Project04
//
//  Created by Roman Brazhnikov on 25/03/2019.
//  Copyright © 2019 Roman Brazhnikov. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {

    var webSites = ["apple.com", "hackingwithswift.com"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return webSites.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "standardCell", for: indexPath)

        // Configuring the cell
        cell.textLabel?.text = webSites[indexPath.row]
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let vc = storyboard?.instantiateViewController(withIdentifier: "webViewController") as? ViewController {
            vc.siteToVisit = webSites[indexPath.row]
            vc.webSites = webSites
            navigationController?.pushViewController(vc, animated: true)
        }
    }


}
